import json
import requests
import re


data = {"participants": []}

for league in "AB":
	res = requests.get(f"https://nerc.itmo.ru/school/archive/2021-2022/roi/standings-{league}.html")
	res.encoding = "utf-8"
	for line in res.text.partition("<tbody>")[2].split("\n")[:-1]:
		groups = re.findall(r"<td .*?>(.*?)</td>", line)

		name, city, form = groups[1].split(", ")
		form = int(form[1:].split()[0])

		probs = [re.sub(r"<.*?>", "", s) for s in groups[2:10]]
		probs = [None if prob == "." else int(prob) for prob in probs]

		data["participants"].append({
			"name": name,
			"form": form,
			"city": city,
			"rank": probs
		})
		# print(name, place, form, probs)
	# print(res.text)


with open("public/index.html", "w") as f:
	f.write("""<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Результаты финального этапа 2022 года</title>

		<style type="text/css">
			body {
				font-family: "Arial", sans-serif;
			}

			h1 {
				text-align: center;
				font-size: 26px;
				color: #18a;
			}

			#loading {
				text-align: center;
				margin-bottom: 100vh;
			}
			p {
				text-align: center;
			}

			a {
				color: #08f;
				/*text-decoration: none;*/
			}
			a:hover, a:focus {
				text-decoration: underline;
			}

			.top-container {
				display: flex;
				justify-content: space-between;
			}

			.oldlinks {
				margin-right: 16px;
			}
			
			.byauthor {
				margin-left: 16px;
				margin-top: 13px;
			}

			table {
				margin: 0 auto;
				border-collapse: collapse;
				margin-bottom: 64px;
			}

			tr {
				height: 40px;
			}
			tr:nth-child(2n) {
				background-color: #f8f8f8;
			}
			.parity {
				background-color: #e0f0ff;
			}
			.parity:nth-child(2n) {
				background-color: #efffff;
			}

			/*
			.automatic {
				background-color: #ffe0d8;
			}
			.automatic:nth-child(2n) {
				background-color: #fff0e8;
			}
			*/

			td {
				padding: 0 6px;
				box-sizing: border-box;
			}
			th:hover a {
				text-decoration: underline;
			}
			.th-link {
				cursor: pointer;
			}

			.count-same {
				text-align: right;
				padding-right: 16px;
				background-color: #fff;
				border: none;
				text-decoration: underline;
				width: 52px;
			}
			.count-same-filled {
			}
			.position {
				text-align: center;
				width: 52px;
			}
			.unreliable-source {
				position: relative;
				color: #a00;
				font-size: 0.8em;
				margin-left: 8px;
				margin-right: 4px;
				top: -6px;
			}
			.partname {
				width: 600px;
			}
			.user-link-prefix {
				margin-left: 8px;
				margin-right: 8px;
			}
			.form {
				text-align: center;
				width: 52px;
			}
			.taskvalue {
				text-align: center;
				width: 40px;
			}
			.taskvalue-full {
				color: #0a0;
				font-weight: bold;
			}
			.taskvalue-zero {
				color: #a00;
			}
			.taskvalue-total {
				font-weight: bold;
			}
			.pass {
				width: 130px;
				background-color: #fff;
			}
			.pass-red {
				color: #a00;
			}
			.pass-gold {
				color: #d80;
			}
			.pass-silver {
				color: #678;
			}

			tfoot .partname {
				text-align: left;
			}

			.ad {
				display: flex;
				flex-direction: column;
				font-size: 10px;
				margin: 48px auto;
				max-width: 100%;
			}

			.ad img {
				max-width: 100%;
			}
			.ad .note {
				text-align: right;
				font-size: 14px;
				color: #808080;
			}

			img {
				max-width: 100%;
				margin: 0 auto;
			}
		</style>
	</head>
	<body>
		<div class="top-container">
			<p class="byauthor">
				Made with ❤️ by Ivanq
			</p>
			<p class="oldlinks">
			</p>
		</div>

		<h1>Финальный этап Всероссийской олимпиады школьников по информатике 2021-2022</h1>

		<p>
			Фильтр:
			<select id="class_filter">
				<option value="all">все классы</option>
				<option value="1,2,3,4,5,6,7,8,9,10">9-10 класс</option>
				<option value="11">11 класс</option>
			</select>
			<select id="city_filter">
				<option value="all">все регионы</option>
			</select>
		</p>

		<table id="data" style="display: none">
			<thead>
				<tr>
					<th></th>
					<th class="position">№</th>
					<th class="partname">Участник</th>
					<th class="form">Класс</th>
					<th class="taskvalue th-link">
						<a href="A1.pdf" title="Сумма расстояний">A1</a>
					</th>
					<th class="taskvalue th-link">
						<a href="B1.pdf" title="Оптимизация закупок">B1</a>
					</th>
					<th class="taskvalue th-link">
						<a href="C1.pdf" title="Интересные выходные">C1</a>
					</th>
					<th class="taskvalue th-link">
						<a href="D1.pdf" title="Прожекторы">D1</a>
					</th>
					<th class="taskvalue th-link">
						<a href="A2.pdf" title="Оптимизация поисковой выдачи">A2</a>
					</th>
					<th class="taskvalue th-link">
						<a href="B2.pdf" title="Экспедиция на Сириус">B2</a>
					</th>
					<th class="taskvalue th-link">
						<a href="C2.pdf" title="Тяжелый груз">C2</a>
					</th>
					<th class="taskvalue th-link">
						<a href="D2.pdf" title="Большие вызовы">D2</a>
					</th>
					<th class="taskvalue">&Sigma;</th>
					<th class="taskvalue th-link">
						<a href="analysis.pdf">Разбор</a>
					</th>
				</tr>
			</thead>
			<tbody id="data_body">
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th class="position"></th>
					<th class="partname">Успешные посылки</th>
					<th class="form"></th>
					<th class="taskvalue submissions-entry">
						1
					</th>
					<th class="taskvalue submissions-entry">
						2
					</th>
					<th class="taskvalue submissions-entry">
						3
					</th>
					<th class="taskvalue submissions-entry">
						4
					</th>
					<th class="taskvalue submissions-entry">
						5
					</th>
					<th class="taskvalue submissions-entry">
						6
					</th>
					<th class="taskvalue submissions-entry">
						7
					</th>
					<th class="taskvalue submissions-entry">
						8
					</th>
					<th class="taskvalue submissions-entry-total">Итого</th>
				</tr>
			</tfoot>
		</table>

		<hr>
		<div id="loading">Loading...</div>
		<p>
			Контакт с создателем: <a href="https://t.me/prog_cf_bot">@prog_cf_bot</a>
		</p>

		<script type="text/javascript">
			const BLOCK_SIZE = 500;

			for(const th of document.querySelectorAll(".th-link")) {
				th.addEventListener("click", e => {
					e.target.querySelector("a").click();
				});
			}


			let abortDataShowing = () => {};


			async function showData(data) {
				abortDataShowing();
				abortDataShowing = () => {};

				let successes = [0, 0, 0, 0, 0, 0, 0, 0];
				for(const row of data) {
					if(row.rank) {
						for(let i = 0; i < 8; i++) {
							if(row.rank[i] == 100) {
								successes[i]++;
							}
						}
					}
				}
				const lst = document.querySelectorAll(".submissions-entry");
				for(let i = 0; i < 8; i++) {
					lst[i].textContent = successes[i];
				}
				document.querySelector(".submissions-entry-total").textContent = successes.reduce((a, b) => a + b, 0);


				document.querySelector("#data_body").innerHTML = "";

				let showI = 0;
				let nextShowI = 0;
				let physI = 0;
				let cnt = 0;
				let parity = true;
				let i = 0;

				let fragment = null;

				for(const row of data) {
					if(i % BLOCK_SIZE == 0) {
						if(fragment) {
							document.querySelector("#data_body").appendChild(fragment);
						}
						fragment = document.createDocumentFragment();
						await new Promise((resolve, reject) => {
							abortDataShowing = reject;
							requestAnimationFrame(resolve);
						});
						abortDataShowing = () => {};
					}

					const node = document.createElement("tr");
					if(parity) {
						node.className = "parity";
					}
					if(row.automatic) {
						node.classList.add("automatic");
					}

					function addTd(value, className) {
						const td = document.createElement("td");
						td.className = className;
						td.textContent = value;
						node.appendChild(td);
						return td;
					}

					if(!row.disqual) {
						cnt++;
					}

					const last = physI === data.length - 1 || data[physI].sumRank !== data[physI + 1].sumRank || data[physI + 1].disqual;

					const fill = last && cnt > 1 && !data[physI].disqual;
					addTd(fill ? cnt : "", "count-same" + (fill ? " count-same-filled" : ""));

					addTd(row.disqual ? "—" : showI + 1, "position");
					const td = addTd(`${row.name} (${row.city})`, "partname");
					if(row.ad) {
						const span = document.createElement("span");
						span.innerHTML = "&mdash;";
						span.className = "user-link-prefix";
						td.appendChild(span);

						const link = document.createElement(row.ad.link ? "a" : "span");
						link.textContent = row.ad.text;
						if(row.ad.link) {
							link.href = row.ad.link;
						}
						link.className = "user-link";
						td.appendChild(link);
					}
					addTd(row.form === 0 ? "" : row.form, "form");
					for(const x of row.rank) {
						addTd(x < 0 ? "" : x, "taskvalue" + ({0: " taskvalue-zero", 100: " taskvalue-full"}[x] || ""));
					}
					addTd(row.sumRank, "taskvalue taskvalue-total");

					if(row.sumRank >= 570) {
						addTd("Победитель", "pass pass-gold");
					} else if(row.sumRank >= 400) {
						addTd("Призер", "pass pass-silver");
					} else {
						addTd("", "pass");
					}

					fragment.appendChild(node);

					physI++;
					if(!row.disqual) {
						nextShowI++;
					}
					if(last) {
						showI = nextShowI;
						parity = !parity;
						cnt = 0;
					}

					i++;
				}

				if(fragment) {
					document.querySelector("#data_body").appendChild(fragment);
				}
			}


			let data = [];
			(async () => {
				const json = """ + json.dumps(data) + """;

				data = json.participants || json;
				for(const row of data) {
					row.sumRank = row.rank.map(x => Math.max(x, 0)).reduce((a, b) => a + b, 0);
				}
				data.sort((a, b) => {
					if(a.sumRank !== b.sumRank) {
						return b.sumRank - a.sumRank;
					}
					if(a.disqual !== b.disqual) {
						return a.disqual - b.disqual;
					}
					if(a.automatic !== b.automatic) {
						return a.automatic - b.automatic;
					}
					if(a.form !== b.form) {
						return a.form - b.form;
					}
					return a.name.localeCompare(b.name);
				});


				let cities = Array.from(new Set(data.map(row => row.city)));
				const topCities = ["город Москва", "город Санкт-Петербург", "Республика Татарстан", "Челябинская область"];
				cities.sort((a, b) => a.localeCompare(b));
				cities = topCities.concat(cities.filter(x => topCities.indexOf(x) === -1));

				for(const city of cities) {
					const node = document.createElement("option");
					node.value = city;
					node.textContent = city;
					document.querySelector("#city_filter").appendChild(node);
				}

				document.querySelector("#loading").style.display = "none";
				document.querySelector("#data").style.display = "";

				requestAnimationFrame(() => showData(data));
			})();


			let classFilter = "all";
			let cityFilter = "all";

			function showFilteredData() {
				let filteredData = data;
				if(classFilter !== "all") {
					const forms = classFilter.split(",").map(form => parseInt(form));
					filteredData = filteredData.filter(row => forms.indexOf(row.form) !== -1);
				}
				if(cityFilter !== "all") {
					filteredData = filteredData.filter(row => row.city === cityFilter);
				}
				showData(filteredData);
			}

			document.querySelector("#class_filter").value = "all";
			document.querySelector("#class_filter").addEventListener("change", e => {
				classFilter = e.target.value;
				showFilteredData();
			});

			document.querySelector("#city_filter").addEventListener("change", e => {
				cityFilter = e.target.value;
				showFilteredData();
			});
		</script>
	</body>
</html>""")
